# README #

Warning.or.kr 를 뚫는 라이브러리. 

![shot-2013-11-15-10.38.36.png](https://bitbucket.org/repo/KRnq9r/images/3075069986-shot-2013-11-15-10.38.36.png)

### What is this repository for? ###

* Warning.or.kr 을 뚫는 라이브러리.
* urllib2, httplib 를 평소처럼 사용하면 됨. 


### Warning ###

* 이 라이브러리의 사용/배포/fork 에 따른 모든 법적인 책임은 사용자가 진다.
* 대략 95%정도의 웹싸이트는 뚫린다.. 하지만 우회하는것이 아니므로, 매우 빠르고 간편하다.

### 설치방법 

```
#!python

python setup.py install
```


### 사용방법 예제 

url 이라는 변수에다가.. 차단된 싸이트주소를 넣어보세요.
예를 들어서

http://xxx.com 

차단된 싸이트가 마법처럼 풀립니다. 헤헤
```
#!python

from cookielib import CookieJar
from hacked_http import urllib2
import webbrowser

url = "" # <== put the restricted url 

cj = CookieJar()
opener = urllib2.build_opener(urllib2.HTTPCookieProcessor(cj))
response = opener.open(url)

with open('test.html', 'wt') as f:
    f.write(response.read())
    
webbrowser.open("test.html")

```


### 파이파이 ###
[https://pypi.python.org/pypi/hacked_http/0.0.1](https://pypi.python.org/pypi/hacked_http/0.0.1)

### 개발자 ###
* 데이터마이닝/웹개발/안드로이드앱/빅데이터 를 전문적으로 하는 개발자입니다.
* 현재 MKET에서 근무하고 있으며, HBI연구소에서 파이썬 기초를 강의하고 있습니다.


이메일 : [a141890@gmail.com](a141890@gmail.com)

웹싸이트 : http://mket.biz

